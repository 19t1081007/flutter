// import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:products/model/ProductsModel.dart';
import 'package:products/productcart.dart';
import 'package:products/products_Page.dart';
import 'package:badges/badges.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'Cart.dart' as cart;

class ProductsDetail extends StatefulWidget {
  ProductsDetail({Key? key, required this.data}) : super(key: key);
  final ProductsModel data;

  @override
  State<ProductsDetail> createState() => _ProductsDetailState();
}

class _ProductsDetailState extends State<ProductsDetail> {
  int tmp = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProductPage(),
                    ));
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        backgroundColor: Color.fromARGB(255, 242, 241, 239),
        actions: [
          Badge(
            badgeContent: Text(
              cart.listCart.length.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            position: const BadgePosition(start: 30, bottom: 30),
            badgeColor: Color.fromARGB(255, 219, 148, 148),
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Color.fromARGB(255, 45, 44, 44),
                size: 30.0,
              ),
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Productcart()),
                ),
              },
            ),
          ),
          const SizedBox(
            width: 25,
          ),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: 500,
            padding: const EdgeInsets.all(30),
            color: Colors.white,
            child: Column(
              children: [
                Center(
                  child: Image.network(
                    widget.data.image.toString(),
                    height: 350,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Container(
                  height: 45,
                ),
                Text(
                  widget.data.title.toString().toUpperCase(),
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Container(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\$${widget.data.price.toString()}",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w900),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 231, 228, 228),
                        borderRadius: BorderRadius.circular(15),
                        // border: Border.all(width: 1, color: Colors.black),
                      ),
                      child: Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  if (tmp > 1) tmp--;
                                });
                              },
                              icon: Icon(Icons.remove)),
                          Text(
                            tmp.toString(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          IconButton(
                              onPressed: () {
                                setState(() {
                                  tmp++;
                                });
                              },
                              icon: Icon(Icons.add)),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  height: 20,
                ),
                Row(
                  children: [
                    Text(
                      "Description",
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                Container(
                  height: 10,
                ),
                Text(
                  widget.data.description.toString(),
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      fontSize: 15, color: Color.fromARGB(255, 103, 103, 103)),
                ),
                Container(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RatingBar(
                      initialRating: widget.data.rating.rate,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 25,
                      ratingWidget: RatingWidget(
                        full: Icon(Icons.star),
                        half: Icon(Icons.star_half),
                        empty: Icon(Icons.star_border_purple500_sharp),
                      ),
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      onRatingUpdate: (rating) {
                        print(rating);
                      },
                    ),
                    Text(
                      "(${widget.data.rating.count.toString()} review)    ",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                    )
                  ],
                ),
                Container(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.only(left: 120, right: 120, bottom: 7),
        child: SizedBox(
          width: 120,
          height: 45,
          child: ElevatedButton.icon(
            icon: Icon(
              Icons.shopping_cart,
              color: Colors.orange,
            ),
            label: Text(
              "Buy",
              style: TextStyle(fontSize: 17),
            ),
            onPressed: () {
              setState(() {
                cart.remove(widget.data);
                cart.add(widget.data, tmp);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Productcart()),
                );
              });
            },
            style: ElevatedButton.styleFrom(
              primary:
                  // Color.fromARGB(255, 242, 241, 239),
                  Color.fromARGB(255, 26, 26, 26),
              shape: StadiumBorder(),

              // padding: EdgeInsets.all(25),
            ),
          ),
        ),
      ),
    );
  }
}
