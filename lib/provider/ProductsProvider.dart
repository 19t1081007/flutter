import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:products/model/ProductsModel.dart';

class ProductsProvider extends ChangeNotifier {
  List<ProductsModel> list = [];
  Future<void> getList() async {
    String apiURL = "https://fakestoreapi.com/products";
    var client = http.Client();
    var jsonString = await client.get(Uri.parse(apiURL));
    var jsonObject = jsonDecode(jsonString.body);
    var productslistObject = jsonObject as List;
    list = productslistObject.map((e) {
      return ProductsModel.fromJson(e);
    }).toList();
    notifyListeners();
  }

  Future<void> getProductsByCate(String category) async {
    String apiURL = "https://fakestoreapi.com/products/category/$category";
    var client = http.Client();
    var jsonString = await client.get(Uri.parse(apiURL));
    var jsonObject = jsonDecode(jsonString.body);
    var productslistObject = jsonObject as List;
    list = productslistObject.map((e) {
      return ProductsModel.fromJson(e);
    }).toList();
    notifyListeners();
  }

  void searchProduct(String input) {
    list = list
        .where((e) =>
            e.title.toString().toLowerCase().contains(input.toLowerCase()))
        .toList();
  }

  void isToPrice(bool ex) {
    if (!ex) {
      list.sort((a, b) => a.price.compareTo(b.price));
    } else {
      list.sort((a, b) => b.price.compareTo(a.price));
    }
  }
}
