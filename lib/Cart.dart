library Cart;

import 'package:products/model/CartsModel.dart';
import 'package:products/model/ProductsModel.dart';

List<CartsModel> listCart = [];
void add(ProductsModel product, int sl) {
  CartsModel Cart = new CartsModel(model: product, quatity: sl);
  for (int i = 0; i < listCart.length; i++) {
    if (product.id == listCart[i].model.id) {
      listCart[i].quatity += sl;
      return;
    }
  }
  listCart.add(Cart);
}

void remove(ProductsModel product) {
  listCart.removeWhere((item) => item.model.id == product.id);
}

void addQuatity(ProductsModel product) {
  for (int i = 0; i < listCart.length; i++) {
    if (product.id == listCart[i].model.id) {
      listCart[i].quatity++;
      return;
    }
  }
}

void removeQuatity(ProductsModel product) {
  for (int i = 0; i < listCart.length; i++) {
    if (product.id == listCart[i].model.id) {
      if (listCart[i].quatity > 1) listCart[i].quatity--;
      return;
    }
  }
}

double totalT(List<CartsModel> listCart) {
  double total = 0;
  for (int i = 0; i < listCart.length; i++) {
    total += (listCart[i].model.price * listCart[i].quatity);
  }
  return total;
}
