import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:products/ProductsDetail.dart';
import 'package:products/model/ProductsModel.dart';
import 'package:products/productcart.dart';
import 'package:products/provider/CategoriesProvider.dart';
import 'package:products/provider/ProductsProvider.dart';
import 'package:provider/provider.dart';
import 'package:badges/badges.dart';
import 'model/CategoriesModel.dart';
import 'Cart.dart' as cart;

String genreSelected = "All";
bool showGrid = true;
bool downPrice = true;
bool isLoading = true;

class ProductPage extends StatefulWidget {
  const ProductPage({super.key});

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    var productsProvider = Provider.of<ProductsProvider>(context);
    var categoriesProvider = Provider.of<CategoriesProvider>(context);
    categoriesProvider.getList();
    if (isLoading) {
      (() async {
        await productsProvider.getList();
        setState(() {
          isLoading = false;
        });
      })();
    }

    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.menu,
                color: Colors.black,
              ),
              onPressed: () {},
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        title: Text(
          "    Products",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Color.fromARGB(255, 242, 241, 239),
        actions: [
          Badge(
            badgeContent: Text(
              cart.listCart.length.toString(),
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            position: const BadgePosition(start: 30, bottom: 30),
            badgeColor: Color.fromARGB(255, 219, 148, 148),
            child: IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Color.fromARGB(255, 45, 44, 44),
                size: 30.0,
              ),
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Productcart()),
                ),
              },
            ),
          ),
          const SizedBox(
            width: 25,
          ),
        ],
      ),
      body: Container(
        width: 500,
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 242, 241, 239),
        ),
        child: Column(
          children: [
            buildTitle(context),
            // buildSearch(context),
            Container(
              padding: const EdgeInsets.only(left: 30, top: 20, right: 10),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      // controller: TextEditingController(),
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.search,
                          size: 30,
                        ),
                        hintText: 'Search...',
                        hintStyle: TextStyle(
                            fontWeight: FontWeight.w600, color: Colors.black),
                      ),
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                      onChanged: (value) {
                        (() async {
                          await productsProvider.getList();
                          setState(() {
                            productsProvider.searchProduct(value);
                          });
                        })();
                      },
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        showGrid = !showGrid;
                      });
                    },
                    icon:
                        showGrid ? Icon(Icons.grid_view) : Icon(Icons.list_alt),
                    iconSize: 28,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        if (downPrice)
                          downPrice = false;
                        else
                          downPrice = true;
                        productsProvider.isToPrice(downPrice);
                      });
                    },
                    child: Row(
                      children: [
                        Text(
                          "Price ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ),
                        Icon(
                          downPrice ? Icons.arrow_downward : Icons.arrow_upward,
                          size: 15,
                        ),
                      ],
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(255, 242, 241, 239),
                      onPrimary: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            // buildCategory(context, categoriesProvider.list),
            Container(
              margin: const EdgeInsets.only(left: 25, top: 20, right: 20),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    TextButton(
                      child: Text(
                        "All",
                        style: TextStyle(
                            fontSize: 18,
                            color: genreSelected == "All"
                                ? Colors.black
                                : Color.fromARGB(255, 128, 121, 121),
                            fontWeight: FontWeight.bold),
                      ),
                      onPressed: () {
                        setState(() {
                          genreSelected = "All";
                          productsProvider.getList();
                        });
                      },
                    ),
                    ...categoriesProvider.list.map((e) {
                      return TextButton(
                        child: Text(
                          e.Category.toString(),
                          style: TextStyle(
                              fontSize: 18,
                              color: (genreSelected == e.Category.toString())
                                  ? Colors.black
                                  : Color.fromARGB(255, 128, 121, 121),
                              fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          setState(() {
                            genreSelected = e.Category.toString();
                            productsProvider.getProductsByCate(genreSelected);
                          });
                        },
                      );
                    }).toList()
                  ],
                ),
              ),
            ),
            showGrid
                ? buildGrid(context, productsProvider.list)
                : buildList(context, productsProvider.list),
          ],
        ),
      ),
    );
  }

  buildTitle(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 70, top: 30),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                "What Would",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          Row(
            children: [
              Text(
                "You Like?",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
              ),
            ],
          )
        ],
      ),
    );
  }

  // buildSearch(BuildContext context) {
  //   return Container(
  //     padding: const EdgeInsets.only(left: 30, top: 20, right: 10),
  //     child: Row(
  //       children: [
  //         Expanded(
  //           child: TextField(
  //             controller: TextEditingController(),
  //             decoration: InputDecoration(
  //               prefixIcon: Icon(
  //                 Icons.search,
  //                 size: 30,
  //               ),
  //               hintText: 'Search...',
  //               hintStyle:
  //                   TextStyle(fontWeight: FontWeight.w500, color: Colors.black),
  //             ),
  //             // onChanged: searchProduct,
  //           ),
  //         ),
  //         IconButton(
  //           onPressed: () {
  //             setState(() {
  //               if (showGrid)
  //                 showGrid = false;
  //               else
  //                 showGrid = true;
  //             });
  //           },
  //           icon: showGrid ? Icon(Icons.grid_view) : Icon(Icons.list_alt),
  //           iconSize: 28,
  //         ),
  //         ElevatedButton(
  //           onPressed: () {
  //             setState(() {
  //               if (downPrice)
  //                 downPrice = false;
  //               else
  //                 downPrice = true;
  //             });
  //           },
  //           child: Row(
  //             children: [
  //               Text(
  //                 "Price ",
  //                 style: TextStyle(fontWeight: FontWeight.bold),
  //               ),
  //               Icon(
  //                 downPrice ? Icons.arrow_downward : Icons.arrow_upward,
  //                 size: 15,
  //               ),
  //             ],
  //           ),
  //           style: ElevatedButton.styleFrom(
  //             primary: Color.fromARGB(255, 242, 241, 239),
  //             onPrimary: Colors.black,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  // buildCategory(BuildContext context, List<CategoriesModel> list) {
  //   return Container(
  //     margin: const EdgeInsets.only(left: 25, top: 20, right: 20),
  //     child: SingleChildScrollView(
  //       scrollDirection: Axis.horizontal,
  //       child: Row(
  //         mainAxisSize: MainAxisSize.max,
  //         children: [
  //           TextButton(
  //             child: Text(
  //               "All",
  //               style: TextStyle(
  //                   fontSize: 18,
  //                   color: genreSelected == "All"
  //                       ? Colors.black
  //                       : Color.fromARGB(255, 128, 121, 121),
  //                   fontWeight: FontWeight.bold),
  //             ),
  //             onPressed: () {
  //               setState(() {
  //                 genreSelected = "All";
  //                 productsProvider.getList();
  //               });
  //             },
  //           ),
  //           ...list.map((e) {
  //             return TextButton(
  //               child: Text(
  //                 e.Category.toString(),
  //                 style: TextStyle(
  //                     fontSize: 18,
  //                     color: (genreSelected == e.Category ||
  //                             genreSelected == "category")
  //                         ? Colors.black
  //                         : Color.fromARGB(255, 128, 121, 121),
  //                     fontWeight: FontWeight.bold),
  //               ),
  //               onPressed: () {
  //                 setState(() {
  //                   genreSelected = e.Category.toString();
  //                 });
  //               },
  //             );
  //           }).toList()
  //         ],
  //       ),
  //     ),
  //   );
  // }

  buildGrid(BuildContext context, List<ProductsModel> list) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2.4;
    final double itemWidth = size.width / 2;
    return Container(
      child: Expanded(
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                margin: const EdgeInsets.only(top: 20, left: 25, right: 25),
                child: GridView.count(
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  crossAxisCount: 2,
                  childAspectRatio: (itemWidth / itemHeight),
                  children: [
                    ...list.map((e) {
                      return ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProductsDetail(data: e)),
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white, // background
                          onPrimary: Colors.black, // foreground
                          padding: EdgeInsets.only(
                              top: 20, bottom: 15, left: 15, right: 15),
                          side: BorderSide(width: 2, color: Colors.white),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        child: Column(
                          children: [
                            Image.network(
                              e.image.toString(),
                              height: 120,
                              fit: BoxFit.fitWidth,
                            ),
                            Container(
                              height: 8,
                            ),
                            SizedBox(
                              height: 35,
                              child: Text(
                                e.title.toString(),
                                maxLines: 2,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Text(
                                  e.rating.rate.toString(),
                                  style: TextStyle(fontSize: 15),
                                ),
                                Container(
                                  width: 3,
                                ),
                                Icon(
                                  Icons.star_border,
                                  size: 18,
                                )
                              ],
                            ),
                            Container(
                              height: 5,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "\$${e.price.toString()}",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      cart.add(e, 1);
                                    });
                                  },
                                  child: Icon(
                                    Icons.shopping_bag_outlined,
                                    size: 23,
                                    color: Colors.black,
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    primary:
                                        // Color.fromARGB(255, 242, 241, 239),
                                        Color.fromARGB(255, 255, 226, 169),
                                    shape: CircleBorder(),
                                    padding: EdgeInsets.all(17),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      );
                      // );
                    }).toList()
                  ],
                ),
              ),
      ),
    );
  }

  buildList(BuildContext context, List<ProductsModel> list) {
    return Container(
      child: Expanded(
        child: Container(
          margin: const EdgeInsets.only(top: 20, left: 15, right: 15),
          child: ListView(
            children: [
              ...list.map((e) {
                return Container(
                  height: 120,
                  margin: EdgeInsets.only(bottom: 20),
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductsDetail(data: e)),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Colors.white, // background
                      onPrimary: Colors.black, // foreground
                      side: BorderSide(width: 2, color: Colors.white),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                    child: Container(
                      padding: const EdgeInsets.only(top: 15, bottom: 10),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 60,
                            height: 80,
                            child: Image.network(
                              e.image.toString(),
                              fit: BoxFit.fitHeight,
                            ),
                          ),
                          Container(
                            width: 15,
                          ),
                          Container(
                            width: 190,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  e.title.toString(),
                                  maxLines: 2,
                                  style: TextStyle(
                                      // fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                Container(
                                  height: 5,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      e.rating.rate.toString(),
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    Container(
                                      width: 3,
                                    ),
                                    Icon(
                                      Icons.star_border,
                                      size: 18,
                                    )
                                  ],
                                ),
                                Container(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "\$${e.price.toString()}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {
                              setState(() {
                                cart.add(e, 1);
                              });
                            },
                            child: Icon(
                              Icons.shopping_bag_outlined,
                              size: 23,
                              color: Colors.black,
                            ),
                            style: ElevatedButton.styleFrom(
                              primary:
                                  // Color.fromARGB(255, 242, 241, 239),
                                  Color.fromARGB(255, 255, 226, 169),
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(17),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
