import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart';
import 'package:products/model/CategoriesModel.dart';

class CategoriesProvider extends ChangeNotifier {
  List<CategoriesModel> list = [];
  Future<void> getList() async {
    String apiURL = "https://fakestoreapi.com/products/categories";
    var client = http.Client();
    var jsonString = await client.get(Uri.parse(apiURL));
    var jsonObject = jsonDecode(jsonString.body);
    var categorieslistObject = jsonObject as List;
    list = categorieslistObject.map((e) {
      return CategoriesModel.formJson(e);
    }).toList();
    notifyListeners();
  }
}
