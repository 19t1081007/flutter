import 'package:flutter/foundation.dart';

class rat {
  double rate;
  int count;

  rat({
    required this.rate,
    required this.count,
  });
  factory rat.fromJson(Map<String, dynamic> obj) {
    return rat(rate: obj['rate'], count: obj['count']);
  }
}

class ProductsModel {
  int id;
  String title;
  double price;
  String description;
  String category;
  String image;
  rat rating;

  ProductsModel({
    required this.id,
    required this.title,
    required this.price,
    required this.description,
    required this.category,
    required this.image,
    required this.rating,
  });

  factory ProductsModel.fromJson(Map<String, dynamic> obj) {
    return ProductsModel(
      id: obj['id'],
      title: obj['title'],
      price: obj['price'],
      description: obj['description'],
      category: obj['category'],
      image: obj['image'],
      rating: rat.fromJson(obj['rating']),
    );
  }
}
