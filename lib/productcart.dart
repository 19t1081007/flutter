import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:products/products_Page.dart';
import 'package:products/provider/ProductsProvider.dart';
import 'package:provider/provider.dart';
import 'package:badges/badges.dart';
import 'Cart.dart' as cart;
import 'package:flutter_slidable/flutter_slidable.dart';

class Productcart extends StatefulWidget {
  const Productcart({super.key});

  @override
  State<Productcart> createState() => _ProductcartState();
}

class _ProductcartState extends State<Productcart> {
  @override
  Widget build(BuildContext context) {
    var productsProvider = Provider.of<ProductsProvider>(context);

    return Scaffold(
      appBar: AppBar(
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProductPage(),
                    ));
              },
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            );
          },
        ),
        backgroundColor: Color.fromARGB(255, 242, 241, 239),
        actions: [
          Icon(
            Icons.web_stories,
            color: Colors.black,
            size: 30,
          ),
          const SizedBox(
            width: 25,
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 242, 241, 239),
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Text(
                "Checkout",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 20,
              ),
              items(context, productsProvider),
              pay(context),
            ],
          ),
        ),
      ),
    );
  }

  items(BuildContext context, productsProvider) {
    return Container(
      child: Expanded(
        child: Container(
          child: ListView(
            children: [
              ...cart.listCart.map((e) {
                return Container(
                  height: 120,
                  margin: const EdgeInsets.only(top: 20),
                  child: Slidable(
                    endActionPane: ActionPane(
                      motion: StretchMotion(),
                      children: [
                        SlidableAction(
                          onPressed: (context) {
                            setState(() {
                              cart.remove(e.model);
                            });
                          },
                          icon: Icons.delete,
                          backgroundColor: Colors.redAccent,
                        )
                      ],
                    ),
                    child: Container(
                      padding:
                          const EdgeInsets.only(left: 10, top: 15, bottom: 10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 60,
                              height: 80,
                              child: Image.network(
                                e.model.image.toString(),
                                fit: BoxFit.fitHeight,
                              ),
                            ),
                            Container(
                              width: 5,
                            ),
                            Container(
                              width: 165,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    e.model.title.toString(),
                                    maxLines: 2,
                                    style: TextStyle(
                                        // fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  Container(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(e.model.category.toString()),
                                      Container(
                                        width: 5,
                                      )
                                    ],
                                  ),
                                  Container(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        // "\$${e.price.toString()}",
                                        "\$${(e.model.price * e.quatity).toStringAsFixed(2)}",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  IconButton(
                                      onPressed: () {
                                        setState(() {
                                          cart.removeQuatity(e.model);
                                        });
                                      },
                                      icon: Icon(
                                        Icons.remove,
                                      )),
                                  Text(
                                    e.quatity.toString(),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  IconButton(
                                      onPressed: () {
                                        setState(() {
                                          cart.addQuatity(e.model);
                                        });
                                      },
                                      icon: Icon(
                                        Icons.add,
                                      )),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }),
            ],
          ),
        ),
      ),
    );
  }

  pay(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 30,
          ),
          Text(
            "- - - - - - - - - - - - - - - - - - - - - -",
            style: TextStyle(fontSize: 30, color: Colors.grey),
          ),
          Container(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Comment on the order",
                style: TextStyle(color: Colors.grey, fontSize: 15),
              ),
              Text(
                "No Comment",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              )
            ],
          ),
          Container(
            height: 35,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  children: [
                    Text(
                      "Total price",
                      maxLines: 1,
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                    ),
                    Text(
                      "\$${cart.totalT(cart.listCart).toStringAsFixed(2)}",
                      style:
                          TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 150,
                height: 55,
                child: ElevatedButton.icon(
                  icon: Icon(
                    Icons.shopping_cart,
                    color: Colors.orange,
                  ),
                  label: Text(
                    "Pay Now",
                    style: TextStyle(fontSize: 17),
                  ),
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    primary:
                        // Color.fromARGB(255, 242, 241, 239),
                        Colors.black,
                    shape: StadiumBorder(),

                    // padding: EdgeInsets.all(25),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
