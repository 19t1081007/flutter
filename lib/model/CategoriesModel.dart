class CategoriesModel {
  String? Category;
  CategoriesModel({this.Category});
  factory CategoriesModel.formJson(String obj) {
    return CategoriesModel(Category: obj);
  }
}
