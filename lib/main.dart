import 'package:flutter/material.dart';
import 'package:products/productcart.dart';
import 'package:products/products_Page.dart';
import 'package:products/provider/CategoriesProvider.dart';
import 'package:products/provider/ProductsProvider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ProductsProvider()),
        ChangeNotifierProvider(create: (_) => CategoriesProvider())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: ProductPage(),
      ),
    ),
  );
}
